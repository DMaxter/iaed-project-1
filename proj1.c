/*
 * Daniel Maximiano Matos
 * Number: 89429
 * 
 * Interface for user input
 */

#include <stdio.h>
#include <string.h>
#include "sparse.h" /* Sparse operations and definition */

int main(int argc, char *argv[]){
	unsigned long matrixInd = 0; /* Index of first free position in the matrix */
	/* limits = { minLines, minColumns, maxLines, maxColumns } */
	unsigned long limits[4] = {ULONG_MAX, ULONG_MAX, 0, 0};
	unsigned long size; /* Size of the matrix */
	double ZERO = DEF_ZERO;
	Sparse matrix[MAX];
	char filename[FILENAME];
	char c; /* Command selector */

	if(argc == 2){
		strcpy(filename, argv[1]);
		matrixInd = loadFromFile(matrix, filename, limits);
	}
	
	do{
		c = getchar();
		switch(c){
			case 'a':
				matrixInd += addElement(matrix, matrixInd, limits, ZERO);	
				break;
			case 'p':
				listElements(matrix, matrixInd);
				break;				
			case 'i':
				if(matrixInd != 0){
					size = (limits[2] - limits[0] + 1) * (limits[3] - limits[1] + 1);
					
					printf("[%lu %lu] [%lu %lu] %lu / %lu = %.3f%%\n", limits[0], 
						limits[1], limits[2], limits[3], matrixInd, size, 
						((float) matrixInd / size) * 100);
				}else{
					printf("empty matrix\n");
				}
				break;
			case 'l': 
				printLine(matrix, matrixInd, limits, ZERO);
				break;
			case 'c':
				printColumn(matrix, matrixInd, limits, ZERO);
				break;
			case 'o':
				sort(matrix, matrixInd, ZERO);
				break;
			case 'z':
				scanf(" %lf", &ZERO);
				matrixInd -= removeZERO(matrix, matrixInd, limits, ZERO);
				break;
			case 's':
				compress(matrix, matrixInd, limits, ZERO);
				break;
			case 'w':
				writeToFile(matrix, matrixInd, ZERO, filename);
				break;
		}
	}while(c != 'q');

	return 0;
}
