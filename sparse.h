/*
 * Daniel Maximiano Matos
 * Number: 89429
 *
 * Public functions and structures
 */

#include <limits.h>

/* Default value for ZERO */
#define DEF_ZERO 0 
/* Maximum number of elements in Sparse matrix */
#define MAX 10000 
/* 80 characters for name PLUS the terminator character */
#define FILENAME 80 + 1 

/* Sparse type definition */
typedef struct{
	unsigned long line, column;
	double value;
} Sparse;

/* Density type definitionn */
typedef struct{
	unsigned long line;
	unsigned long count; /* Number of elements in each line */
} Density;

/* Add/Remove element to/from the given matrix */
int addElement(Sparse matrix[], unsigned long index, unsigned long limits[4], double ZERO);
/* Search for element on given line column */
unsigned long searchElement(Sparse matrix[], unsigned long index, unsigned long line, unsigned long column);
/* Remove all ZERO's in the given matrix */
unsigned long removeZERO(Sparse matrix[], unsigned long index, unsigned long limits[4], double ZERO);
/* Print all elements of the given matrix */
void listElements(Sparse matrix[], unsigned long index);
/* Scan for a line and print all its elements */
void printLine(Sparse matrix[], unsigned long index, unsigned long limits[4], double ZERO);
/* Scan for a column and print all its elements */
void printColumn(Sparse matrix[], unsigned long index, unsigned long limits[4], double ZERO);
/* Sort by lines/column */
void sort(Sparse matrix[], unsigned long index, double ZERO);
/* Save matrix to given file */
void writeToFile(Sparse matrix[], unsigned long index, double ZERO, char filename[FILENAME]);
/* Load matrix from file */
unsigned long loadFromFile(Sparse matrix[], char filename[FILENAME], unsigned long limits[4]);
/* Compress matrix */
void compress(Sparse matrix[], unsigned long index, unsigned long limits[4], double ZERO);
/* Create a copy of a Sparse array */
void copySparse(Sparse src[], unsigned long index, Sparse dest[]);
/* Calculate the density of each line */
unsigned long lineDensity(Sparse matrix[], unsigned long index, Density density[MAX]);
/* Search for the first element in the given line *r the first element in the given line */
unsigned long searchLine(Sparse matrix[], unsigned long index, unsigned long line);
