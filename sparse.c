/*
 * Daniel Maximiano Matos
 * Number: 89429
 *
 * Sparse manipulation
 */

#include <stdio.h>
#include <stdlib.h>
#include "sparse.h"

/* PRIVATE FUNCTIONS */

/*
 * Function: lessLine
 * -------------------
 * Sort lines by ascending order, if lines are equal, sort columns by ascending
 * order (case lines and columns are both equal is impossible)
 *
 * elem1: one Sparse element
 * elem2: the other Sparse element
 *
 * returns: 1 if first element is bigger than the second
 *	    -1 if first element is smaller than the second
 */
int lessLine(const void * elem1, const void * elem2){
	Sparse *p1 = (Sparse *) elem1;
	Sparse *p2 = (Sparse *) elem2;

	return ((p1->line > p2->line) ||
		((p1->line == p2->line) && (p1->column > p2->column))) ? 1 : -1;
}

/*
 * Function: lessColumn
 * -------------------
 * Sort columns by ascending order, if columns are equal, sort lines by 
 * ascending order (case lines and columns are both equal is impossible)
 *
 * elem1: one Sparse element
 * elem2: the other Sparse element
 *
 * returns: 1 if first element is bigger than the second
 *	    -1 if first element is smaller than the second
 */
int lessColumn(const void * elem1, const void * elem2){
	Sparse *p1 = (Sparse *) elem1;
	Sparse *p2 = (Sparse *) elem2;
	return ((p1->column > p2->column) ||
		((p1->column == p2->column) && (p1->line > p2->line))) ? 1 : -1;
}

/*
 * Function: moreDensity
 * ---------------------
 * Sort density by descending order, if density of two lines is equal, sort
 * lines by ascending order (case two lines are equal is impossible)
 *
 * elem1: one Density element
 * elem2: the other Density element
 *
 * returns: 1 if first element is smaller than the second
 *	    -1 if first element is bigger than the second
 */
int moreDensity(const void * elem1, const void * elem2){
	Density *p1 = (Density *) elem1;
	Density *p2 = (Density *) elem2;
	return ((p1->count < p2->count) ||
		((p1->count == p2->count) && (p1->line > p2->line))) ? 1 : -1;
}

/*
 * Function: addElement
 * --------------------
 * Add/Remove element to/from matrix and update limits
 *
 * matrix: Sparse array
 * index: length of matrix
 * limits: array with minimum and maximum of the columns and of the lines of
 *		the matrix
 * ZERO: current value that is not represented on matrix
 *
 * return: 1 if one element was added
 *	   0 if one element was modified
 *	   number of elements removed (negative number)
 */
int addElement(Sparse matrix[], unsigned long index, unsigned long limits[4],
		double ZERO){
	unsigned long line, column, tmpInd;
	double value;
	int ret; 

	ret = scanf(" %lu %lu %lf", &line, &column, &value);
	
	if(ret != 3 || index >= MAX){
		return 0;
	}

	tmpInd = searchElement(matrix, index, line, column);

	matrix[tmpInd].line = line;
	matrix[tmpInd].column = column;
	matrix[tmpInd].value = value;

	if(value == ZERO){
		return -removeZERO(matrix, index, limits, ZERO);
	}

	/* Update limits */
	if(line < limits[0]){
		limits[0] = line;
	}
	if(line > limits[2]){
		limits[2] = line;
	}

	if(column < limits[1]){
		limits[1] = column;
	}
	if(column > limits[3]){
		limits[3] = column;
	}
	
	return (tmpInd == index) ? 1 : 0;
}

/*
 * Function: searchElement
 * -----------------------
 * Search the matrix for the element on the given line and column
 *
 * matrix: Sparse array
 * index: length of matrix
 * line: line to search
 * column: column to search
 *
 * returns: the value of index if element is not found
 *	    the index where is the element of that line and column
 */
unsigned long searchElement(Sparse matrix[], unsigned long index,
				 unsigned long line, unsigned long column){
	unsigned long i;

	for(i = 0; i < index; i++){
		if(matrix[i].line == line && matrix[i].column == column){
			return i;
		}
	}

	return index;
}

/*
 * Function: removeZERO
 * --------------------
 * Remove the elements of the matrix with the value ZERO and update limits
 *
 * matrix: Sparse array
 * index: length of matrix
 * limits: array with minimum and maximum of the columns and of the lines of
 *		the matrix
 * ZERO: current value that is not represented on matrix
 *
 * returns: the number of removed elements
 */
unsigned long removeZERO(Sparse matrix[], unsigned long index,
				unsigned long limits[4], double ZERO){
	unsigned long i, j; /* Read and Write cursors, respectively */
	
	/* Reset limits */
	limits[0] = limits[1] = ULONG_MAX; 
	limits[2] = limits[3] = 0;

	for(i = j = 0; i < index; i++){
		if(matrix[i].value == ZERO){
			continue;
		}
		
		/* Update limits */
		if(matrix[i].line < limits[0]){
			limits[0] = matrix[i].line;
		}
		if(matrix[i].line > limits[2]){
			limits[2] = matrix[i].line;
		}

		if(matrix[i].column < limits[1]){
			limits[1] = matrix[i].column;
		}
		if(matrix[i].column > limits[3]){
			limits[3] = matrix[i].column;
		}

		matrix[j++] = matrix[i];
	}

	return i-j;
}

/*
 * Function: listElements
 * ----------------------
 * Print all elements of the matrix
 *
 * matrix: Sparse array
 * index: length of matrix
 */
void listElements(Sparse matrix[], unsigned long index){
	unsigned long i;

	if(index == 0){
		printf("empty matrix\n");	
	}

	for(i = 0; i < index; i++){
		printf("[%lu;%lu]=%.3f\n", matrix[i].line, matrix[i].column,
			matrix[i].value);
	}
}

/*
 * Function: printLine
 * -------------------
 * Scan the value of a line and print all of its elements
 *
 * matrix: Sparse array
 * index: length of matrix
 * limits: array with minimum and maximum of the columns and of the lines of
 *		the matrix
 * ZERO: current value that is not represented on matrix
 */
void printLine(Sparse matrix[], unsigned long index, unsigned long limits[4],
		double ZERO){
	unsigned long line;
	unsigned long i, j;
	unsigned long maxJ;
	Sparse values[MAX];
	int ret;	

	ret = scanf(" %lu", &line);

	if(ret != 1){
		return;
	}

	/* Check if is out of limits */
	if(!(limits[0] <= line && line <= limits[2])){
		printf("empty line\n");
		return;
	}

	/* Put the values with the given line on a separate array */
	for(i = maxJ = 0; i < index; i++){
		if(matrix[i].line == line){
			values[maxJ++] = matrix[i];
		}
	}

	if(maxJ == 0){
		printf("empty line\n");
		return;
	}

	/* Sort by column */
	qsort(values, maxJ, sizeof(Sparse), lessColumn);

	for(i = limits[1], j = 0; i <= limits[3]; i++){
		if(values[j].column == i && j < maxJ){
			printf(" %.3f", values[j].value);
			j++;
		}else{
			printf(" %.3f", ZERO);
		}
	}
	
	putchar('\n');
}

/*
 * Function: printColumn
 * -------------------
 * Scan the value of a column and print all of its elements
 *
 * matrix: Sparse array
 * index: length of matrix
 * limits: array with minimum and maximum of the columns and of the lines of
 *		the matrix
 * ZERO: current value that is not represented on matrix
 */
void printColumn(Sparse matrix[], unsigned long index, unsigned long limits[4],
			double ZERO){
	unsigned long column;
	unsigned long i, j;
	unsigned long maxJ;
	Sparse values[MAX];
	int ret;

	ret = scanf(" %lu", &column);

	if(ret != 1){
		return;
	}

	/* Check if is out of limits */
	if(!(limits[1] <= column && column <= limits[3])){
		printf("empty column\n");
		return;
	}

	/* Put the values with the given column on a separate array */
	for(i = maxJ = 0; i < index; i++){
		if(matrix[i].column == column){
			values[maxJ++] = matrix[i];
		}
	}

	if(maxJ == 0){
		printf("empty column\n");
		return;
	}

	/* Sort by line */
	qsort(values, maxJ, sizeof(Sparse), lessLine);

	for(i = limits[0], j = 0; i <= limits[2]; i++){
		if(values[j].line == i && j < maxJ){
			printf("[%lu;%lu]=%.3f\n", values[j].line,
				values[j].column, values[j].value);
			j++;
		}else{
			printf("[%lu;%lu]=%.3f\n", i, column, ZERO);
		}
	}
}

/*
 * Function: sort
 * --------------
 * Sort by lines/columns, depending if input is 'column' or not
 *
 * matrix: Sparse array
 * index: length of matrix
 * ZERO: current value that is not represented on matrix
 */
void sort(Sparse matrix[], unsigned long index, double ZERO){
	char c;
	int i;
	
	c = getchar();

	if(c == '\n'){
		qsort(matrix, index, sizeof(Sparse), lessLine);
	}else{
		c = getchar();

		for(i = 0; "column"[i] != '\0'; i++){
			if(c != "column"[i]){
				return;
			}

			c = getchar();
		}

		qsort(matrix, index, sizeof(Sparse), lessColumn);
	}
	
}

/*
 * Function: writeToFile
 * ---------------------
 * Save matrix to given file
 *
 * matrix: Sparse array
 * index: length of matrix
 * ZERO: current value that is not represented on matrix
 * filename: name of the file to save
 */
void writeToFile(Sparse matrix[], unsigned long index, double ZERO,
			char filename[FILENAME]){
	char c;
	unsigned long i;
	FILE *fp;
	
	c = getchar();
	
	if(c != '\n'){
		scanf(" %s", filename);	
	}

	fp = fopen(filename, "w");
	
	if(fp != NULL){	
		for(i = 0; i < index; i++){
			fprintf(fp, "[%lu,%lu] = %f\n", matrix[i].line, matrix[i].column, matrix[i].value);
		}

		fclose(fp);
	}
}

/*
 * Function: loadFromFile
 * ----------------------
 * Load matrix from file
 *
 * matrix: Sparse array
 * filename: name of the file to read from
 * limits: array with minimum and maximum of the columns and of the lines of
 *		the matrix
 *
 * returns: length of the matrix
 */
unsigned long loadFromFile(Sparse matrix[], char filename[FILENAME],
				unsigned long limits[4]){
	FILE *fp;
	unsigned long index = 0;

	fp = fopen(filename, "r");

	if(fp != NULL){
		while(fscanf(fp, "[%lu,%lu] = %lf\n", &matrix[index].line, 
				&matrix[index].column, &matrix[index].value)
				!= EOF){
			index++;
		}

		/* Remove ZERO from matrix and update limits */
		index -= removeZERO(matrix, index, limits, DEF_ZERO);

		fclose(fp);
	}

	return index;
}

/*
 * Function: compress
 * ------------------
 * Compress the matrix using double-offset indexing
 *
 * matrix: Sparse array
 * matrixInd: length of matrix
 * limits: array with minimum and maximum of the columns and of the lines of
 *		the matrix
 * ZERO: current value that is not represented on matrix
 */
void compress(Sparse matrix[], unsigned long matrixInd, unsigned long limits[4],
		double ZERO){
	/* Calculate the density of the whole matrix */
	double matrixDensity  = ((double) matrixInd / ((limits[2] - limits[0] + 1) * (limits[3] - limits[1] + 1)));
	unsigned long offset[MAX*2] = {0}, index[MAX*2] = {0};
	double values[MAX*2];
	Density density[MAX]; /* Array with density per line */
	unsigned long densInd; /* Length of density array */
	Sparse copy[MAX]; /* Copy of matrix */
	unsigned long i;
	unsigned long maxOffset = 0;
	unsigned long max; /* Maximum limit in various arrays */
	unsigned long lineStart; /* Index with the start of the current line  */
	unsigned long lineOffset; /* Offset of the current line */
	unsigned long count; /* Number of elements in one line */

	if(matrixDensity > 0.5){
		printf("dense matrix\n");
		return;
	}	
	
	/* Initialize the values array with ZERO */
	for(i = 0; i < MAX*2; i++){
		values[i] = ZERO;
	}

	/* Create a copy of the matrix and sort it by line */
	copySparse(matrix, matrixInd, copy);
	qsort(copy, matrixInd, sizeof(Sparse), lessLine);

	/* Count the number of values in each line and sort by density */
	densInd = lineDensity(copy, matrixInd, density);
	qsort(density, densInd, sizeof(Density), moreDensity);

	for(i = 0; i < densInd; i++){
		lineOffset = 0;
		lineStart = searchLine(copy, matrixInd, density[i].line);

		/* Check for empty positions */
		for(count = 0; copy[lineStart].line == density[i].line; lineStart++){
			if(values[copy[lineStart].column - limits[1] + lineOffset] != ZERO){
				lineStart -= count + 1;
				count = 0;
				lineOffset++;
			}else{
				count++;
			}
		}
		
		/* Put values on empty positions */
		for(lineStart -= count; copy[lineStart].line == density[i].line; lineStart++){
			values[copy[lineStart].column - limits[1] + lineOffset] = copy[lineStart].value;
			index[copy[lineStart].column - limits[1] + lineOffset] = copy[lineStart].line;
		}

		offset[density[i].line-limits[0]] = lineOffset;
		
		if(lineOffset > maxOffset){
			maxOffset = lineOffset;
		}
	}

	/* Output values */
	max = limits[3] - limits[1] + 1 + maxOffset;
	printf("value =");

	for(i = 0; i < max; i++){
		printf(" %.3f", values[i]);
	}
	putchar('\n');	

	/* Output index */;
	printf("index =");

	for(i = 0; i < max; i++){
		printf(" %lu", index[i]);
	}
	putchar('\n');

	/* Output offset */
	max = limits[2] - limits[0] + 1;
	printf("offset =");

	for(i = 0; i < max; i++){
		printf(" %lu", offset[i]);
	}
	putchar('\n');
}

/*
 * Function: copySparse
 * --------------------
 * Create a copy of a Sparse matrix
 * 
 * src: Sparse matrix to be copied
 * index: number of elements to copy
 * dest: Sparse matrix to copy
 */
void copySparse(Sparse src[], unsigned long index, Sparse dest[]){
	unsigned long i;
	
	for(i = 0; i < index; i++){
		dest[i] = src[i];
	}
}

/*
 * Function: lineDensity
 * ---------------------
 * Count the number of elements in each line
 *
 * matrix: Sparse array
 * index: length of matrix
 * density: array to receive the density of each line
 *
 * returns: the length of the density array
 */
unsigned long lineDensity(Sparse matrix[], unsigned long index,
				Density density[MAX]){
	unsigned long i, j;

	density[0].line = matrix[0].line;
	density[0].count = 0;

	for(i = j = 0; i < index; i++){
		if(density[j].line == matrix[i].line){
			density[j].count++;
		}else{
			j++;
			density[j].line = matrix[i].line;
			density[j].count = 1;
		}
	}

	return j + 1;
}

/*
 * Function: searchLine
 * --------------------
 * Search for the first element on a line
 *
 * matrix: Sparse array
 * index: length of matrix
 * line: line to search
 *
 * returns: the index of the first element of the given line
 *	    the length of the matrix if element is not found
 */
unsigned long searchLine(Sparse matrix[], unsigned long index,
				unsigned long line){
	unsigned long i;

	for(i = 0; i < index; i++){
		if(matrix[i].line == line){
			return i;
		}	
	}

	return index;
}
